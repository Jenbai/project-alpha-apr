from django.urls import path
from tasks.views import create_task, task_list, create_note

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", task_list, name="show_my_tasks"),
    path("notes/", create_note, name="create_note"),
]
